# OpenCraft Accounting Service

[![pipeline status](https://gitlab.com/opencraft/billing/accounting/badges/master/pipeline.svg)](https://gitlab.com/opencraft/billing/accounting/-/commits/master)
[![coverage report](https://gitlab.com/opencraft/billing/accounting/badges/master/coverage.svg)](https://gitlab.com/opencraft/billing/accounting/-/commits/master)

The OpenCraft automated accounting system that can:

* Register company members and clients who need to bill or be billed.
* Automatically generate invoices using **JIRA worklogs** for incoming invoices.
* Process outgoing payments automatically through **TransferWise**.
* Push JIRA worklogs to **Freshbooks** as line items.
* Create invoices on Freshbooks based off of previously pushed line items.

## Install

The development environment is containerized and running on Docker, hence you
will need to [install Docker](https://www.docker.com/get-started).

Beside Docker, for development, we use [pre-commit](https://pre-commit.com/) for running some quality tests and enforce formatting before committing. Installation of `pre-commit` is **optional**, though it may help your development workflow.

After `pre-commit` is installed, run the following to setup the git hooks and download the required plugins.

```sh
pre-commit install
```

## Usage

### Start services for development

```sh
make dev.up
```

After the services are started, navigate to [http://localhost:1786/](http://localhost:1786/admin) to check the admin interface of the accounting service.

Although the servie can be started by executing `make dev.up`, without proper credentials, it won't be able to connect to the necessary third-party services like Google or Jira.

To set credentials used by the service, in the `.envs/.local` directory create a `.secret` file and add the following:

```
JIRA_SERVER_URL=<YOUR COMPANY JIRA ADDRESS>
JIRA_SERVICE_USER_USERNAME=<SERVICE ACCOUNT USERNAME ACCESSING JIRA>
JIRA_SERVICE_USER_PASSWORD=<SERVICE ACCOUNT PASSWROD ACCESSING JIRA>
GOOGLE_AUTH_CLIENT_SERVICE_EMAIL=<GOOGLE WORKSPACE SERVICE ACCOUNT EMAIL>
GOOGLE_DRIVE_ROOT=<ID OF THE GOOGLE DRIVE ROOT FOLDER USED TO UPLOAD INVOICES>
GOOGLE_AUTH_PKCS12_FILE_PATH=<P12 CREDENTIALS FILE USED TO AUTHENTICATE THE GOOGLE SERVICE USER>
BILLING_CYCLE_USERS=<USERS TO CONSIDER IN THE BILLING CYCLE>
```

### Initial setup

At the beginning, you have a database without any users. In case you would like to create a superuser, run the following command:

```sh
$ docker-compose run --rm django python manage.py createsuperuser
```

### JIRA Worklogs Integration

Invoices can be loaded with line items derived from JIRA Tempo worklogs, given that the username of the provider on the invoice is equal to a username on a JIRA instance.

Synchronization is guaranteed -- any line item that's tagged as a JIRA worklog will be checked against incoming JIRA worklogs, and a mismatch leads to deletion, whereas ones that don't exist yet always get added, thus ensuring a complete match for all relevant fields -- issue title, worklog description, worklog quantity, and so on.

You will also need a JIRA user that serves as a sort of 'service user', but it doesn't have to be -- any user that can access any other user's worklogs through the API is good to be used.

Once you fill up `.envs/.local/.secret` with the proper JIRA variables (making sure to also set `ENABLE_JIRA=true`), you can activate JIRA downloading for individual invoices, and see it work!

### Invoice PDF Generation

Invoice PDFs can be automatically generated given the existing line items.

This feature is orthogonal to other integrations -- it solely depends on what line items exist. So you could add JIRA worklogs automatically, for example, and generated invoice PDFs would contain those worklogs, too, on top of any extra line item you happen to add manually.

To use this feature, you'll need [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html). Download the binary for your development environment, and place the file either in the repository root (it's ignored by git) or somewhere that matches the `HTML_TO_PDF_BINARY_PATH` variable.

### Google Drive Integration

Integration with Google Drive currently depends upon being able to generate invoice PDFs. Once a PDF is generated, the Google Drive Integration can take the resulting file and upload it to some very specific location.

The path expected is as such, given that `year` and `month` are the invoice's creation date year and month in numerical form, respectively:

```text
year -> invoices-in -> month -> invoice.pdf
```

For example:

```text
2021 -> invoices-in -> 09 -> invoice.pdf
```

This would mean that the integration uploaded `invoice.pdf` to folder `09`, inside folder `invoices-in`, which was inside folder `2021`. This also thus corresponds to the invoice's date being `2021-09-X` where `X` is any day in September.

You need to manually create the parent folders (`2021`, `invoices-in`, `09`) for the file to be placed inside.

Given the file path examples above, set `GOOGLE_DRIVE_ROOT` to the ID of the folder that *contains* the year folder, i.e. the one that contains `2021`. The ID for this folder is in the URL when you're actually looking at the folder in your browser. E.g. if you see `https://drive.google.com/drive/u/1/folders/1NLXlnhTY4RcpGQuvFgRNPiv0fYZ3peI7`, then use just `1NLXlnhTY4RcpGQuvFgRNPiv0fYZ3peI7` as the ID.

For your development environment to use Google Drive integration, you'll have to set up a service account on Google Console. This service account can then be associated with PKCS12 credentials, which you can download.

The downloaded PKCS12 credentials can be stored as a file named `.p12` in the root of this repository, or you can write the downloaded file's name in the `GOOGLE_AUTH_PKCS12_FILE_PATH` setting.

You need to let the integration know the service account's email through `GOOGLE_AUTH_CLIENT_SERVICE_EMAIL` -- this information should be obvious from the console when you create the user, or through the JSON credentials file which you have the option of downloading, akin to downloading the PKCS12 file.
`GOOGLE_AUTH_CLIENT_USER_EMAIL` doesn't need to be changed.

Service accounts also need to be given permission to your (presumably private) Google Drive folder.
E.g. if you want invoices to be visible by `someuser@projectname.net`, that user must open the dropdown near the folder name, click `Share with others`, and type the e-mail of the service account (e.g. `bot-name@project-name.iam.gserviceaccount.com`).

Make sure to set `ENABLE_GOOGLE=true`!

### Client Tempo Report

Tempo report from the Jira server can be generated and sent to the client automatically. This module helps to generate the report from Jira. The [Tempo API token](https://www.midori-global.com/products/better-excel-exporter-for-jira/data-center/documentation/integrations/tempo-timesheets) needs to be generated on your Jira server. This needs to be set as `TEMPO_API_TOKEN` setting. The client name and their respective billing accounts need to be configured in the admin panel. The mails are sent out bi-weekly, first mail is sent out on `REPORT_MID_MONTH_DAY` day of month this includes hours from 1st to 15th of the same month and second report is sent out on `REPORT_POST_MID_MONTH_DAY` day of month this includs hours from 16th to the last day of the previous month.

Reports can be configured at https://<site>/admin/invoice/clientinfo/. The admin needs to add `client info` on the page.
Like:

![Admin image about client info](./images/client_info.png)

- `Client Name` is the name of the customer that one wants to appear on the report.
- `Account Names` is a list of account keys seperated by comma, one can get the account key by visiting `<JIRA_SERVER>/secure/TempoAccounts!default.jspa`
and filtering by client name.
- `Client Emails` is a list of emails seperated by comma. Client reports are sent to these emails.
- `Active` this can mark the client to be active, if this is unmarked no emails are sent to the client.


### Add and Manage Tax Components to Invoice Line Items

#### Default tax settings

One or more tax components can be setup for each provider in their hourly rate.

To do this open the concerned hourly rate in django admin and click on `Add another Tax component` under the `TAX COMPONENTS` heading.

Fill in the following :
- `Tax type` The name of the tax such as `VAT`, `GST`, `TDS`, etc.
- `Tax rate` The tax rate in percentage
- `Apply by default` Check this if this tax component is to be added by default to all invoice line items for future invoices. You can configure additional tax components which are not applicable for most invoice line items, and keep this field unchecked. That way, these additional tax components can be manually applied to selected invoice line items.

Click on `SAVE` on the hourly rate page, to save the tax components.

To delete an existing tax component, check the `Delete` checkbox in the upper right side of the respective tax component and click on `SAVE`.

#### Individual invoice line items

All tax components for which the `Apply by default` checkbox is checked, will be applied to all automatically generated invoice line items.

If an invoice line item is manually created, then the tax components applicable for it would have to be manually added (even the default ones).

To add a tax component, open the concerned invoice line item in django admin and click on `Add another Line item tax component` and select the tax component from the dropdown list and click on `SAVE`

**Note:** The dropdown list will only contain tax components that were configured in the corresponding hourly rate.
**Note:** The option to add tax components to line items in visible for only existing line items. This means, this option will not be visible when creating a new line item using the `ADD LINE ITEM` button.

To delete a tax component, check the `Delete` checkbox in the upper right side of the respective tax component and click on `SAVE`.



## Run code formatting

We use automated code formatting to ensure the same coding style. To run the automated formatters, run:

```sh
make format             ## Run formatters.
```

Optionally, if you have `pre-commit` installed, you may want to run `pre-commit` to ensure the code is well-formatted before pushing it. To run `pre-commit`, execute `make test.pre-commit`.

## Run tests

First, you will need to have a running development environment.

In case of using VSCode for running the tests, you will need to export every
environment variables which are in the local .envs.

```sh
export $(cat .envs/.local/.{django,postgres} | grep -v "#" | xargs)
```

Regardless how you run tests, the first is to export the following two variables
within the container.

```sh
export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}"
```

### Running test using VSCode

VSCode has a great support for "remote" development using containers or SSH
connection. The project already defines the remote development settings. For
more information how to set up the remote development environment, please refer
to [VSCode's post](https://code.visualstudio.com/blogs/2019/05/02/remote-development).

Within the remote development container, install the dependencies and run the
test command.

```sh
make install_dev_requirements
make test
```

### Running tests in the container

When you are directly using the service container, you won't have to install
requirements, so you can go on with the test command in the first place.

```sh
make test
```

### Running quality tests

We run quality tests - with different setup - in three ways: CI/CD pipelines,
before committing, and manually. To run quality tests manually, you can run
`make test.quality`, though checking pre-commit tests you may want to run
`make test.pre-commit`. For the latter one you will need to
[install](https://pre-commit.com/#install) `pre-commit`.

```sh
make test.migrations     ## Check if migrations are missing.
make test.quality        ## Run quality tests.

# Optionally you can run the following as well
make test.pre-commit     ## Run pre-commit tests.
```
