# Reference Scripts

This directory contains one-off scripts that are currently used for automation but aren't (yet)
integrated into this project.

The prime example is the script we use to upload Jira Tempo worklogs into Freshbooks. It's a script that's run locally by our CEO after the worklogs are exported from Tempo. One day we'd like to have this functionality integrated into billing, but for now he runs it locally.

Storing them here makes it easier for us to debug them as needed and also stores as a reference for when we begin integrating this functionality.
