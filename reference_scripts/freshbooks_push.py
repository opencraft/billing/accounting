#!/usr/bin/env python

# Imports #####################################################################

import csv
import json
import sys
import time

import requests

# Constants ###################################################################

FRESHBOOKS_CLIENT_ID = "0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a"
FRESHBOOKS_CLIENT_SECRET = "0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a"
FRESHBOOKS_REDIRECT_URI = "https://opencraft.com/"
FRESHBOOKS_AUTH_CODE_URL = (
    "https://my.freshbooks.com/service/auth/oauth/authorize"
    "?client_id={}&response_type=code&redirect_uri={}".format(FRESHBOOKS_CLIENT_ID, FRESHBOOKS_REDIRECT_URI)
)
FRESHBOOKS_TMP_AUTH_FILEPATH = "etc/freshbooks_tmp_auth"
FRESHBOOKS_BUSINESS_ID = {"DE": 11111111, "CA": 1111111112}

ACCOUNT_KEY = "Account Key"
ACCOUNT_NAME = "Account Name"
HOURS = "Hours"
ISSUE_TITLE = "Issue summary"
ISSUE_TYPE = "Issue Type"
PROJECT_KEY = "Project Key"
WORK_DESCRIPTION = "Work Description"


# Functions ###################################################################


def get_freshbooks_new_access_token():
    auth_code = input("Enter auth code from {} \n".format(FRESHBOOKS_AUTH_CODE_URL))

    headers = {"Api-Version": "alpha", "Content-Type": "application/json"}
    payload = {
        "client_id": FRESHBOOKS_CLIENT_ID,
        "client_secret": FRESHBOOKS_CLIENT_SECRET,
        "code": auth_code,
        "grant_type": "authorization_code",
        "redirect_uri": FRESHBOOKS_REDIRECT_URI,
    }
    res = requests.post("https://api.freshbooks.com/auth/oauth/token", data=json.dumps(payload), headers=headers)
    res.raise_for_status()

    with open(FRESHBOOKS_TMP_AUTH_FILEPATH, "w") as f:
        f.write(res.text)
    access_token = res.json()["access_token"]

    return access_token


def test_freshbooks_headers(headers):
    res = requests.get("https://api.freshbooks.com/test", headers=headers)
    return res.status_code == requests.codes.ok


def get_freshbooks_headers():
    def _get_headers(access_token):
        return {
            "Api-Version": "alpha",
            "Authorization": "Bearer {}".format(access_token),
        }

    # Attempt to get a cached value of the access token
    try:
        with open(FRESHBOOKS_TMP_AUTH_FILEPATH) as fp:
            access_token = json.load(fp)["access_token"]
    except (IOError, json.decoder.JSONDecodeError):
        access_token = get_freshbooks_new_access_token()
    else:
        # Test the cached token
        if not test_freshbooks_headers(_get_headers(access_token)):
            access_token = get_freshbooks_new_access_token()

    return _get_headers(access_token)


def get_accounts_worklog(csv_filename):
    accounts_worklog = {"CA": {}, "DE": {}}
    with open(csv_filename, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            company = "DE"
            if row[ACCOUNT_NAME].startswith("(GmbH) - "):
                row[ACCOUNT_NAME] = row[ACCOUNT_NAME][9:]
            elif row[ACCOUNT_NAME].startswith("(Inc) - "):
                row[ACCOUNT_NAME] = row[ACCOUNT_NAME][8:]
                company = "CA"

            if row[ACCOUNT_KEY] not in accounts_worklog[company]:
                accounts_worklog[company][row[ACCOUNT_KEY]] = {
                    "account_name": row[ACCOUNT_NAME],
                    "hours": float(row[HOURS]),
                    "description": row[WORK_DESCRIPTION],
                    "issues": [row[ISSUE_TITLE]],
                }
            else:
                worklog = accounts_worklog[company][row[ACCOUNT_KEY]]
                worklog["hours"] += float(row[HOURS])
                worklog["description"] += "\n- {}".format(row[WORK_DESCRIPTION])
                if row[ISSUE_TITLE] not in worklog["issues"]:
                    worklog["issues"].append(row[ISSUE_TITLE])

    return accounts_worklog


def push_accounts_worklog_to_freshbooks(freshbooks_headers, accounts_worklog, freshbooks_projects, logdate):
    for company, company_worklogs in accounts_worklog.items():
        for worklog in company_worklogs.values():
            project = freshbooks_projects[company][worklog["account_name"]]

            notes = ""
            for issue in worklog["issues"]:
                notes += "- {}\n".format(issue)
            # notes += '\n{}'.format(worklog['description'])
            time_logged = round(worklog["hours"], 1)
            time_logged_seconds = time_logged * 60 * 60

            time_entry = {
                "time_entry": {
                    "is_logged": True,
                    "duration": round(time_logged_seconds, 2),
                    "note": notes,
                    "started_at": "{}T01:00:00.000Z".format(logdate),
                    "client_id": project["client_id"],
                    "project_id": project["id"],
                },
            }
            print(json.dumps(time_entry))

            # This specific POST seem to need the content type specified (but not the POST for the list of projects)
            fbh = freshbooks_headers.copy()
            fbh["Content-Type"] = "application/json"

            url = "https://api.freshbooks.com/timetracking/business/{}/time_entries".format(
                FRESHBOOKS_BUSINESS_ID[company]
            )
            res = requests.post(url, headers=fbh, data=json.dumps(time_entry))

            # Retry in case of server error
            while res.status_code == 500:
                print(res.content)
                time.sleep(1)
                res = requests.post(url, headers=fbh, data=json.dumps(time_entry))

            res.raise_for_status()

    print("DONE: Pushed {} account worklogs to Freshbooks".format(len(accounts_worklog)))


def push_tempo_csv_to_freshbooks(csv_filename, logdate):
    accounts_worklog = get_accounts_worklog(csv_filename)

    freshbooks_headers = get_freshbooks_headers()

    freshbooks_projects = {"CA": {}, "DE": {}}
    has_projects_for_all_accounts = True
    for company, business_id in FRESHBOOKS_BUSINESS_ID.items():
        url = "https://api.freshbooks.com/projects/business/{}/projects?per_page=100".format(business_id)
        res = requests.get(url, data=None, headers=freshbooks_headers)
        res.raise_for_status()
        res_json = res.json()
        res_meta = res_json["meta"]

        # Handle multi-pages results - Download all & concatenate
        page = res_meta["page"] or 0
        while res_meta["pages"] > page:
            page += 1
            sub_url = "{}&page={}".format(url, page)
            sub_res = requests.get(sub_url, data=None, headers=freshbooks_headers)
            sub_res.raise_for_status()
            sub_res_json = sub_res.json()
            res_json["projects"] += sub_res_json["projects"]

        for project in res_json["projects"]:
            freshbooks_projects[company][project["title"]] = project

        for worklog in accounts_worklog[company].values():
            if worklog["account_name"] not in freshbooks_projects[company].keys():
                has_projects_for_all_accounts = False
                print('ERROR: No Freshbook project for {} account "{}"'.format(company, worklog["account_name"]))

    if not has_projects_for_all_accounts:
        print("ERROR: No Freshbook project for some accounts, please update Jira and/or Freshbook")
        sys.exit(1)
    else:
        push_accounts_worklog_to_freshbooks(freshbooks_headers, accounts_worklog, freshbooks_projects, logdate)


# Main ########################################################################

if __name__ == "__main__":
    push_tempo_csv_to_freshbooks("csv/2020/09.csv", "2020-09-02")
