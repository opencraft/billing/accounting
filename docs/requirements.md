# Requirements

This is the list of features we want for the [billing app](../README.md), and their complexity.
Related [discovery task][discovery-task].


## Wishlist

We previously used to gather ideas in this [wishlist document][wishlist], which is public.
Don't use it from now on. There was a decision to use GitLab, so open a merge request or comment on one instead.

## Components and features

This is a list of building blocks grouped by topic. It doesn't reflect the order in which they will be completed.
It can be seen as list of key requirements.

- Start and planning
  - Discovery
  - UX discovery (mockups)
  - Access permissions (including Vault), setup
  - Choose name, adjust domain
  - Dependencies upgrade and basic UI (React)
  - Define workflows (submission, approval, payment, etc.)
  - Plugin architecture, and de-OpenCraft it
  - Easy upgrade and deployment
- Implement user invoice workflows
  - Basic invoice generation and approval. Including validation
  - Custom invoice information (extra lines that need to appear)
  - Invoice regeneration on demand
  - Refundable expenses
  - Customizable numbering schemas
  - Edge cases for basic workflows
  - Onboarding (admin invites, user fills in the rest of the data)
  - Invoice templates and ability to manage templates
- Automatic payments
  - "Wise" (Transfer) CSV
  - Automating as much as possible sending the "Wise" payment to the bank and reacting when the payment is sent
  - Currencies in "Wise"
- Manual control panel (for managers)
  - UI for managers to control all operations (which were automatic up to now)
  - Log viewer and status viewer for several operations
- JIRA→Freshbooks worklog upload for client billing
  - Automatic and manual process to upload to Freshbooks
  - Freshbooks account auto-creation
- Special invoice workflows (see [wishlist][wishlist] for ideas about these concepts)
  - Held payments, and automatic payment after being unheld
  - Split billing (an account, e.g. devops, shared between many clients)
  - Matched worklogs (e.g. core committer)
  - Grouped invoices (many members combined into same invoice)
- Public outreach and multiple organizations (apart from OpenCraft)
  - Allow public registrations
  - "Organization" concept (multi-tenancy)
  - JIRA credentials set up from the web interface
  - Additional security restrictions, roles and permissions management
  - Paid plans
  - Marketing outreach
  - Login options (and not requiring a Google account to log in)

## List of tasks

This is an unordered list of tasks (as opposed to topics or features or ideas) to cover the features ideas from the wishlist. Each one starts with a verb, and discoveries are tagged with DISC. Ideally each task will be a medium piece of work.
This is just a first idea done for estimation purposes and it doesn't represent the actual task list.
However we included the task ID when a task exists.
If you want to add implementation ideas, please create a GitLab issue and add them there. 

- (SE-4256) Set up access / Vault. Grant accesses
- Choose project name. And create a category in our forum (Discourse) forum to discuss this software
- ([SE-4083](https://tasks.opencraft.com/browse/SE-4083) / [GitLab issue 2](https://gitlab.com/opencraft/billing/accounting/-/issues/2)): DISC: Do initial discovery. Includes: finding needs for UX discovery
- (SE-4084 / GitLab issue 3): DISC: Do mockups and UX discovery. Includes: preparing screens
- (SE-4218 / GitLab issue 4): Upgrade dependencies, make the docker 1-line instructions work, and update technical documentation
- Add React and front-end, basic infrastructure, package and release
- Deploy to production (incl. basic front-end and dependencies upgrade), and document and simplify the process (to do it very often)
- (GitLab issue 5): Review invoice upload to Google Docs, fix bugs
- Write client non-technical documentation about our intentions: paid plans, self-hosting, shared usage of our public service, pricing, public registration
- Implement/verify plugin architecture, and decouple OpenCraft logic (and theming) from non-OpenCraft logic. Make JIRA, Google, Freshbooks, Transferwise be optional plug-ins
- Verify the CI in GitLab (CircleCI, or Gitlab CI)
- Set up public web interface, domains and review access control
- Review server’s backups and security
- Improve Django admin interface (many small bugs/additions in the wishlist)
- DISC: Define user workflows (refunds, worklogs, regeneration, payments, onboarding, offboarding, dates, …). Including special workflows (e.g. held payments) though they can be improved later
- Implement normal invoice sending and approval (with UI), including user-editable details
- Implement bank information UI, to be included in the invoice. Including validation of VAT, currency, country (which may require admin approval)
- Implement user-editable invoice details (non-basic information to be included in invoices)
- Implement managing and previewing invoice templates
- Implement invoice regeneration on user request (getting JIRA worklogs again)
- Implement and test customizable invoice numbering schemas
- Fix edge cases and add protections (rounding issues, negatives, zeros, graceful failures, out of order events, abuse etc.) 
- Do a round of user testing and feedback gathering (internal, external). (This will be recurring) 
- Implement USD and EUR in all invoices (internally. Not yet sending through Wise)
- DISC: Find out how to make the Wise payment and how to make it tell us that the payment was executed
- Implement Wise payment initiation (with manual confirmation), or callback (or similar) to be notified when the payment was executed
- Implement status viewer and log viewer to check the status of several operations
- Implement USD/EUR separation in Wise payments
- Implement manager control panel (UI where managers can manually control the otherwise automated billing cycle)
- DISC: finish the workflows for held payments
- Implement held payments, and automatic payment after being unhold
- DISC: plan FreshBooks integration
- Implement FreshBooks upload (rewrite manual script freshbooks_push.py)
- Implement FreshBooks upload launcher, including viewing results
- Implement FreshBooks accounts/projects auto-creation based on JIRA accounts
- DISC: decide general approach about JIRA vs. Freshbook imparity, as needed for split billing and for matched worklogs (concepts described at the [wishlist][wishlist])
- DISC: prepare split billing (account, e.g. devops, shared between many clients)
- Implement split billing (e.g. for devops)
- DISC: prepare how to implement matched worklogs (e.g. core committer)
- Implement matched worklogs UI
- Implement matched worklogs balancing
- Test matched worklogs with real data (core committer)
- DISC: prepare grouped invoices (many members included in same invoice)
- Implement grouped invoices data model
- Apply grouped invoices to Canadian subsidiary case and to Fixate case 
- Implement e-mail to set up bank data after being added/invited by an admin. Interface to send, reception, and first login
- Implement SSO through Google Auth after being invited, so that the user doesn’t use a password
- Implement other non-Google SSO, or some way to use the software without a Google account
- Implement support for time frames for rate validity (i.e. hourly rate is disabled before or after a date and payments are held)
- DISC: discuss public registration and opening the product to other organizations. Including: SSO logins, and restrictions; demo data or demo instance 
- Implement public registration by allowing anonymous users to create accounts with access to demo data
- Implement company-specific UIs where each company (apart from OpenCraft) can define details for the organization (name, members, JIRA keys, SSO keys, e-mail domain for logins, …)
- Implement multi-tenant restrictions to make sure that users from an organization can’t see data from users from another organization. Including: prevent logins that don’t match the organization’s domain
- DISC: decide approach for user onboarding process (documentation or web)
- Implement user onboarding process
- DISC: marketing outreach (listing project, collaborations, contributions)
- Offer the product to other organizations
- DISC: paid plans (pricing, tiers)
- Implement billing for the paid component of this software. Meta-billing!. Manual at first
- DISC: decide other plug-ins to develop (Note: plugin architecture will also be done in all tasks, not only in this task)

## List of screens

This is a preliminary list of visible sections.

Note: the types of users are (for now): anonymous, member, manager, admin. Members can view and modify their own invoices. Managers can initiate payments and control the billing cycle. Admins can do everything. Anonymous nothing.

- Home page (visible to everyone in Internet). It will describe the project. Later it will invite people and organizations to create accounts
- Login form and password reset. Registration initially disabled to the public
- Profile page, where you can edit your password and contact details
- E-mails, that should include some information in the footer. They otherwise include simple messages, links, and attached files
- Admin interface (Django). Might be the place where some of the data is inputted. It includes certain advanced operations (like: holding payments, or enabling “grouped invoices”). Many other actions mentioned here could be integrated and triggered from the admin.
- Interface after you (a member) approve your invoice, and see if it’s already approved
- Interface where you edit custom data that needs to appear in your invoice
- Interface where you edit your bank information, account, name. May be the same as where you add custom data
- Interface where an admin creates an account for a team member: admin sets account and hourly rate, then the new user will set the other fields. 
- [Possibly] Interface where the managers can see the final payments to do, validate them and send the payment to the bank for execution. This could also happen outside only (in the bank).
- Interface where managers can control the billing cycle, for instance to manually generate invoice for a past period (or current period) and for a subset of users (or all of them)
- Interface where managers can create a “grouped invoice” that combines several people (to simplify their accounting). It could be the same interface used to manually create a normal invoice
- Interface where managers can launch an operation to export JIRA work logs into FreshBooks and see the status of the process until it completes 
- [Possibly] Interface where a user (a “core committer”) can see a report of logged time in particular accounts, compare them with worklogs in other accounts, and trigger an operation to balance them 
- Interface where admins and managers can see logs and the status of certain operations
- [Possibly] Interface where a manager can set details about the organization (e.g. OpenCraft): name, members, passwords to JIRA servers, VAT information
- [Possibly] Interface where you can preview an invoice template
- [Possibly] Interface, or addition to existing interface, or documentation, to onboard new users
- Internal APIs

## First estimates (March 2021)

The list of tasks mentioned above includes medium-sized tasks, that could be from 3 to 5 points each in our usual estimation points; let's use 4 points as average. Some tasks may be more complex and others may be simpler but there aren't outliers.
Note that each task will include UI/UX work.

There are 60 tasks listed above. This creates 240 points of work in tasks.

In addition there are at least 19 known screens or sections, let's say 20. Having a screen/section also adds complexity and we'll arbitrarily add 3 more points per screen/section. (60 points). The UX discovery will find more precise estimates for the UX work.

We have until now 300 points of work.
Translating this to hours isn't straightforward, but we can use a range of 3 h/point to 5 h/point, which would give 900 to 1500 h.

In addition there are recurring tasks for project management, UX work, and development. These aren't fixed, they depend on the length of the project, so we can add a percentage, like 15%. These tasks include regular usability reviews, research and feedback gathering, security hardening, user onboarding.
This brings the range to **1035-1725 hours**.
(20% of this comes from the UX/screens part).

The [monthly budget for accelerated projects][new-process-focus] is up to 400 h/cell but it's to be divided between several projects (e.g. SE-4081: workflow manager), and it depends on capacity too.
Let's assume we can use an average of **70 h/month** (it will be less in the beginning due to slow hiring).
Then the full project would take 15 to 25 months (let's use 22 as a compromise, a bit in the middle but tilted towards the higher number).
If we start in April 2021, the deadline would be 22 months after that (**February 2023**).
**This is the main option.**

This estimate considers that we'd implement all features. In reality we can decide on the go what to implement each sprint.
Most probably we'll implement a subset of all ideas, plus a few new ideas.
Not all the features are equally important for a first version (MVP) that others can use.
We could arbitrarily set an earlier deadline for MVP v1 (e.g. when we complete 85% of the features).

Alternative: let's use 100 h/month. Then it will be complete in 10 to 17 months, with a pretty complete MVP v1 in around 8 months.

Depending on team capacity we can go towards 70 or towards 100 h/month. The deadline will also depend on it. If we want a strict deadline that we can stick to independently of hiring, let's start assuming the slower rate (70 h/month) and if we can hire faster we'll be able to finish faster.
As of March 2021, 70 h/month still seems high (hiring will need time to reach that capacity). [Ref.](https://chat.opencraft.com/opencraft/pl/s7brsghddjnxipfakptzc3ip8h). We'll discuss it again when planning April's capacity..

<!-- LINKS -->

[wishlist]: https://docs.google.com/document/d/1cAYNG7YOrhscy8rlxjOIAils2f5kuTmFNYuGNwvsaiI/edit#
[discovery-task]: https://gitlab.com/opencraft/billing/accounting/-/issues/2
[new-process-focus]: https://docs.google.com/document/d/1bkxTJ_otvrqlyqrqS03nAoH0pxGou9NQ2ZiCrWBazFQ/edit#heading=h.dkpbz3ww4zxw
