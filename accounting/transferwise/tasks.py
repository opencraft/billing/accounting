# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
TransferWise tasks.
"""

from django.conf import settings
from django.core.mail.message import EmailMessage
from huey import crontab

from accounting.account.models import Account
from accounting.common.periodic_task_notification import periodic_task_notify
from accounting.transferwise import models


@periodic_task_notify(crontab(day=settings.TRANSFERWISE_BULK_PAYMENT_DAY, hour="0", minute="0"))
def send_bulk_payment_csv():
    """ Generate a bulk payment CSV, send it to the designated TransferWise Bulk Payment sender via e-mail,
    and upload it to Google Drive. Do this for each configure sender. """
    # Make the new bulk payment for the senders.
    for sender_username in settings.TRANSFERWISE_BULK_PAYMENT_SENDER:
        sender = Account.objects.get(user__username=sender_username)
        
        bulk_payment = models.TransferWiseBulkPayment.objects.create(sender=sender)
        bulk_payment.create_payments()
        bulk_payment_csv = bulk_payment.to_bulk_payment_csv()

        # Make the email, attach the CSV and send.
        email = EmailMessage(
            subject='TransferWise Bulk Payment CSV',
            body='TransferWise Bulk Payment CSV is attached.',
            to=[bulk_payment.sender.user.email],
        )
        email.attach_file(bulk_payment_csv.name)
        email.send()

        target_path=[bulk_payment.date.strftime('%Y'), 'invoices-in', bulk_payment.date.strftime('%m')]
        if bulk_payment.sender.file_upload_directory:
            # Insert sender specific directory name in file path if available
            target_path.insert(0, bulk_payment.sender.file_upload_directory)

        # Upload to GDrive.
        file = bulk_payment.upload_to_google_drive(
            file_path=bulk_payment.csv_path,
            target_path=target_path,
            title=bulk_payment.csv_filename,
        )
        bulk_payment.csv_path = file['alternateLink']
        bulk_payment.save()
