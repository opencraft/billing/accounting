import logging
import traceback

from django.conf import settings
from django.core.mail.message import EmailMessage
from functools import wraps
from huey.contrib.djhuey import db_periodic_task

LOGGER = logging.getLogger(__name__)


def notify_on_failure(fn):
    """
    Send notification if a periodic task fails
    """
    @wraps(fn)
    def inner(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as ex:
            LOGGER.warn(f"Exception while running task {fn.__name__}: {ex}")
            send_email_notification(fn.__name__, traceback.format_exc())
            LOGGER.warn(f"Notification Sent")
    return inner


def send_email_notification(task_name, trace_back):
    """
    Render failed job email and send.
    """
    message = f"The periodic task '{task_name}' failed with the following error :\n\n{trace_back}"

    to_emails = settings.NOTIFY_FAILED_JOB_EMAIL.split(',')

    email = EmailMessage(
        subject=f"Periodic account job {task_name} failed",
        body=message,
        to=to_emails,
    )
    email.send()


def periodic_task_notify(*args, **kwargs):
    """
    Wrapper around db_periodic_task function to notify failed tasks
    """
    def decorator(fn):
        return db_periodic_task(*args, **kwargs)(notify_on_failure(fn))
    return decorator