# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Admin for the Bank application.
"""

from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from jsonschema.exceptions import ValidationError as JsonValidationError

from accounting.bank import models
from accounting.bank.utils import validate_identification
from accounting.common.admin import UuidModelAdmin


@admin.register(models.Bank)
class BankAdmin(UuidModelAdmin):
    """ Admin configuration for the `Bank` model. """
    list_display = UuidModelAdmin.list_display + ('name', 'address',)
    search_fields = ('name',)

class BankAccountAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    class Meta:
        model = models.BankAccount
        fields = '__all__'

    def clean_identification(self):
        identification = self.cleaned_data.get("identification")
        try:
            identification = validate_identification(identification)
            return identification
        except JsonValidationError as error:
            if len(error.path):
                error.message = f"{error.path.pop()} : {error.message}"
            raise ValidationError(error.message)

@admin.register(models.BankAccount)
class BankAccountAdmin(UuidModelAdmin):
    """ Admin configuration for the `BankAccount` model. """
    form = BankAccountAdminForm
    list_display = UuidModelAdmin.list_display + ('bank', 'user_account', 'type',)
    raw_id_fields = ('bank',)
