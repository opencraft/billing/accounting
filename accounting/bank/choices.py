# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Bank choices.
"""

from django.utils.translation import ugettext_lazy as _
from djchoices import ChoiceItem, DjangoChoices
from enum import Enum


class BankAccountIdentifiers(Enum):
    """
    Choices for bank account identification.

    This matches what TransferWise works with for bank identification.
    It should be kept up to date depending on the API version used.
    """
    BIC_SWIFT = {
        "name": _('BIC/SWIFT'),
        "field_name": 'bic_swift',
        "type": "string", 
        "required": False
    }

    ACCOUNT_NUMBER = {
        "name": _('Account Number'),
        "field_name": 'account_number',
        "type": "string",
        "required": True
    }
    CURRENCY = {
        "name": _('Currency'),
        "field_name": 'currency',
        "type": "string",
        "required": True
    }


class BankAccountType(DjangoChoices):
    """
    Choices for bank account types.
    """
    CHECKING = ChoiceItem('checking', _('Checking Account'))
    SAVINGS = ChoiceItem('savings', _('Savings Account'))
