# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Bank application utilities.
"""

from accounting.bank.choices import BankAccountIdentifiers
import jsonschema


def identification_schema():
    """
    Produce a simple bank account identification dictionary schema based off of some choices.
    """
    return {identifier.value['field_name']: '' for identifier in BankAccountIdentifiers}

def validate_identification(data):
    json_validation_schema = {"type": "object", "properties": {}, "required": []}
    for identifier in BankAccountIdentifiers:
        if identifier.value['required']:
            field_name = identifier.value['field_name']
            json_validation_schema["properties"][field_name] = {
                "type": identifier.value['type'],
                "minLength": 1
            }
            json_validation_schema["required"].append(field_name)
            value = data.get(field_name, None)
            if value and isinstance(value, str):
                data[field_name] = value.strip()

    jsonschema.validate(data, json_validation_schema)
    return data