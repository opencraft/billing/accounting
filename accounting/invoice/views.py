# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Invoice views.
"""

from urllib.parse import urljoin
import datetime

from django.conf import settings
from django.contrib.sites.models import Site
from django.template.loader import get_template
from rest_framework import decorators, permissions, response, viewsets
from rest_framework.reverse import reverse

from accounting.invoice import choices, constants, models
from accounting.invoice.utils import regenerate_invoice, render_and_send_invoice_email


class InvoiceViewSet(viewsets.ModelViewSet):
    """
    A view for retrieving and updating `Invoice` objects.
    """

    permission_classes = (permissions.IsAdminUser,)
    queryset = models.Invoice.objects.all()
    lookup_field = 'uuid'

    @decorators.detail_route(
        permission_classes=[permissions.AllowAny],
        queryset=models.Invoice.objects.filter(approved=choices.InvoiceApproval.not_approved)
    )
    def approve(self, request, uuid=None):
        """
        Approve the invoice with UUID `uuid`.
        """
        invoice = self.get_object()
        invoice.approved = choices.InvoiceApproval.manually
        invoice.save()
        return response.Response({'approved': True})

    @decorators.detail_route(
        permission_classes=[permissions.AllowAny],
        queryset=models.Invoice.objects.filter(paid=False, approved=choices.InvoiceApproval.not_approved)
    )
    def regenerate(self, request, uuid=None):
        """
        Regenerate the invoice with UUID `uuid`.
        """
        # Regenerate given invoice
        invoice = self.get_object()
        regenerate_invoice(invoice)

        # Send email to provider with the regenerated invoice attached
        base_site_url = Site.objects.get_current().domain
        render_and_send_invoice_email(
            get_template(constants.INVOICE_APPROVAL_TEMPLATE),
            constants.INVOICE_APPROVAL_SUBJECT,
            invoice,
            extra_email_context={
                'approval_url': lambda invoice: urljoin(
                    base_site_url,
                    reverse('invoice:invoice-approve', [invoice.uuid])
                ),
                'regenerate_invoice_url': lambda invoice: urljoin(
                    base_site_url,
                    reverse('invoice:invoice-regenerate', [invoice.uuid])
                ),
                'final_date': datetime.datetime.now().replace(day=int(settings.INVOICE_FINAL_DAY))
            },
        )

        return response.Response({'invoice_regenerated': True})

    @decorators.detail_route(queryset=models.Invoice.objects.filter(paid=False))
    def pay(self, request, uuid=None):
        """
        Pay the invoice with UUID `uuid`.
        """
        invoice = self.get_object()
        invoice.paid = True
        invoice.save()
        return response.Response({
            'paid': True,
            'total': invoice.total_cost,
            'currency': invoice.hourly_rate.hourly_rate_currency
        })
