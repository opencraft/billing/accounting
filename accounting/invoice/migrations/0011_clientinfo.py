# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2023-09-17 12:49
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields

import accounting.common.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0010_auto_20180628_1326'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('client_name', models.TextField(help_text='The name of the client.')),
                ('account_names', models.TextField(help_text='The account names used by the client. Multiple account names should be separated by a comma.')),
                ('client_emails', models.TextField(help_text='The email addresses of the client. Multiple emails should be seperated by comma.')),
                ('active', models.BooleanField(default=True, help_text='Whether this client information is active or not.')),
            ],
            options={
                'abstract': False,
            },
            bases=(accounting.common.mixins.ValidateModelMixin, models.Model),
        ),
    ]
