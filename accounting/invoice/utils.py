# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Invoice utilities.
"""

import datetime
import logging

from django.conf import settings
from django.core.mail.message import EmailMessage
from django.template.loader import get_template

from accounting.account.models import Account
from accounting.common.utils import get_last_day_past_month
from accounting.invoice import choices, models
from accounting.invoice.client_report import PDFReportGenerator, Report

LOGGER = logging.getLogger(__name__)


def resolve_dict_callables(dictionary, *args):
    """
    Resolve any potential callables in a dictionary by passing it some arguments.

    We use a new dictionary to prevent mutating the callable. If we didn't, then on another round of calling this
    with the same dictionary, the callable would have disappeared.

    Example without using a new dictionary:
    >>> d = {
    >>>     'a': lambda a: a**2
    >>>     'b': 'str'
    >>> }
    >>> resolve_dict_callables(d, 4)
    >>> print(d)
    >>> # We lose the callable.
    >>> {'a': 16, 'b': 'str'}
    """
    new_dict = {}
    for key, potential_callable in dictionary.items():
        if callable(potential_callable):
            new_dict[key] = potential_callable(*args)
        else:
            new_dict[key] = dictionary[key]
    return new_dict


def upload_invoice_to_google_drive(invoice, draft_invoice=False):
    """
    Upload an invoice to Google Drive.

    :param invoice: The invoice to upload.
    :param draft_invoice: Whether this invoice should be uploaded into a 'draft' directory.
    :return:
    """
    target_path = [invoice.date.strftime('%Y'), 'invoices-in', invoice.date.strftime('%m')]
    if invoice.client.file_upload_directory:
        # Insert client specific directory name in file path if available
        target_path.insert(0, invoice.client.file_upload_directory)
    if draft_invoice:
        # Insert a draft folder before the last invoice-containing folder.
        target_path.insert(-1, 'draft')
    file = invoice.upload_to_google_drive(
        file_path=invoice.pdf_path,
        target_path=target_path,
        title=invoice.pdf_filename,
        delete_duplicate=True,
    )
    invoice.pdf_path = file['alternateLink']
    invoice.save()


def regenerate_invoice(invoice):
    """
    Regenerate invoice and upload to drive.

    This function is to be used by providers and admins to regenerate
    invoices. This is useful when new line items such as reimburesments are
    to be added, or new JIRA logs need to synced and included in the invoice.
    """

    # Sync line items with JIRA
    invoice.fill_line_items_from_jira()
    invoice.to_pdf()
    upload_invoice_to_google_drive(invoice, draft_invoice=not invoice.is_approved)


def render_and_send_invoice_email(template, subject, invoice, extra_email_context=None):
    """
    Render invoice email using given template and contexts, attach invoice and send.
    """
    if extra_email_context is None:
        extra_email_context = {}

    # Get billing month from invoice
    past = invoice.billing_end_date
    past_month_formatted = past.strftime('%B')

    # Render email header and body
    message_context = {'month': past_month_formatted, 'contact_email': invoice.client.user.email}
    message_context.update(**resolve_dict_callables(extra_email_context, invoice))
    message = template.render(message_context)
    email = EmailMessage(
        subject=subject.format(month=past_month_formatted),
        body=message,
        to=[invoice.provider.user.email],
        cc=[invoice.client.user.email],
    )
    email.attach_file(invoice.to_pdf())
    email.send()


# pylint: disable=too-many-arguments,too-many-locals
def send_email_with_invoice(template, subject, extra_email_context=None, client_usernames=settings.BILLING_CYCLE_USERS,
                            create_invoice=False, draft_invoice=False, fill_line_items_from_jira=False,
                            upload_to_google_drive=False, auto_approve=False):
    """
    Send an email with an invoice attached, and potentially perform peripheral actions.

    TODO: This function's pretty beefy; break it up by encapsulating logic into more utility functions or a new class.

    :param template: The email template.
    :param subject: The email subject.
    :param extra_email_context: Extra kwargs to update the email context with.
    :param client_usernames: The list of billing cycle users' usernames.
    :param create_invoice: Whether to create a new invoice based off of a past one.
    :param draft_invoice: Whether this is a draft invoice or not.
    :param fill_line_items_from_jira: Whether to fill up the invoice's line items from JIRA worklogs.
    :param upload_to_google_drive: Whether to upload the resulting invoice PDF to Google Drive.
                                   If using a draft invoice, it gets uploaded into `invoices-in/draft/{month}`.
    """
    now = datetime.datetime.now()
    past = get_last_day_past_month()
    template = get_template(template)
    for client_username in client_usernames:
        client = Account.objects.get(user__username=client_username)
        providers = [rate.provider for rate in client.client_hourly_rates.filter(active=True)]
        for provider in providers:
            try:
                invoice = models.Invoice.objects.filter(
                    provider=provider,
                    client=client,
                    date__month=past.month if draft_invoice else now.month,
                ).latest('date')
            except models.Invoice.DoesNotExist:
                # This may be the provider's first invoice.
                invoice = None

            if create_invoice or not invoice:
                invoice = models.Invoice.objects.create(
                    number=choices.InvoiceNumberingScheme.increment_value(
                        invoice.template.numbering_scheme,
                        invoice.number
                    ) if invoice else choices.InvoiceNumberingScheme.default_value(
                        provider.invoice_template.numbering_scheme
                    ),
                    provider=provider,
                    client=client,
                    template=provider.invoice_template,
                )
            if fill_line_items_from_jira:
                invoice.fill_line_items_from_jira()

            # Make the email, attach the invoice, and send it.
            render_and_send_invoice_email(
                template=template,
                subject=subject,
                invoice=invoice,
                extra_email_context=extra_email_context
            )

            # Upload the invoice to a directory in GDrive.
            if upload_to_google_drive:
                upload_invoice_to_google_drive(invoice, draft_invoice=draft_invoice)
            if auto_approve and not invoice.is_approved:
                invoice.approved = choices.InvoiceApproval.automatically
            invoice.save()


def render_and_send_report_email(template, subject, report, from_date,
                                 to_date, client_emails, pdf_path):
    """
    Render report email using given template and contexts, attach report and send.
    """

    # Render email header and body
    message_context = {'from_date': from_date,
                       'to_date': to_date,
                       'contact_email': settings.REPORT_CC_EMAIL}
    message = template.render(message_context)
    email = EmailMessage(
        subject=subject.format(client_name=report.client_name, from_date=from_date, to_date=to_date),
        body=message,
        to=client_emails,
        cc=[settings.REPORT_CC_EMAIL],
    )
    email.attach_file(pdf_path)
    email.send()


def send_hour_report_to_clients(from_date, to_date, template, subject):
    """ This function will fetch all active clients, generate the report and send a report email to each client.
    """
    clients = models.ClientInfo.objects.filter(active=True)
    template = get_template(template)
    for client in clients:
        try:
            name = client.client_name
            emails = client.client_emails.split(',')
            accounts = client.account_names.split(',')
            report = Report(from_date, to_date, name, accounts)
            report_pdf = PDFReportGenerator(report).generate()
            if report_pdf:
                render_and_send_report_email(template, subject, report, from_date, to_date, emails, report_pdf)
        except Exception as e:  # pylint: disable=broad-except
            LOGGER.exception("An error occurred while generating and sending the report email for client: %s", name)
