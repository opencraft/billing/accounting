# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2023 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

""" This module is responsible to generate a client report from Jira Tempo.
    It will generate a PDF report for a given client and date range.
"""

from urllib.parse import urlencode
import logging
import os
import uuid
import xml.etree.ElementTree as ET

from django.conf import settings
from django.contrib.sites.models import Site
from django.template.loader import get_template
import pdfkit
import requests

LOGGER = logging.getLogger(__name__)


class BillingAccount:
    """ Class responsible for holding the billing account information.
    """
    def __init__(self, client_name, account_key, account_name, total_hours) -> None:
        self.client_name = client_name
        self.account_key = account_key
        self.account_name = account_name
        self.total_hours = total_hours
        self.tickets = []

    def __str__(self) -> str:
        return f"{self.client_name} - {self.account_name} - {self.total_hours}"

    def add_tickets(self, ticket):
        """ This function will add the tickets to the billing account.
        """
        self.tickets.extend(ticket)


class Tickets:
    """ Class responsible for holding the ticket information.
    """
    def __init__(self, ticket_key, ticket_summary, ticket_hours) -> None:
        self.key = ticket_key
        self.summary = ticket_summary
        self.hours = ticket_hours

    def __str__(self) -> str:
        return f"{self.key} - {self.summary} - {self.hours}"


class Report:
    """ Class responsible for generating the report.
    """
    def __init__(self, from_date, to_date, client_name, account_keys) -> None:
        self.from_date = from_date
        self.to_date = to_date
        self.client_name = client_name
        self.account_keys = account_keys

    def _get_jira_xml_report(self, account_name):
        """ This function will generate the JIRA XML report for the given account name
            and date ranges.
        """
        response = None
        # Date should be in the format of YYYY-MM-DD and passed as a string.
        query_params = {
            'dateFrom': self.from_date,
            'dateTo': self.to_date,
            'billingKey': account_name,
            'useExternalHours': 'false',
            'tempoApiToken': settings.TEMPO_API_TOKEN
        }
        query_string = urlencode(query_params)
        jira_url = f"{settings.JIRA_SERVER_URL}/plugins/servlet/tempo-getWorklogReport/?{query_string}"
        response = requests.get(jira_url)
        if response.status_code == 200:
            response = response.content
        else:
            LOGGER.info('Jira report call failed for %s', account_name)
        return response

    def fetch_report_information(self):
        """ This function will fetch the report information from JIRA and return
            a list of BillingAccount objects.
        """
        accounts = []
        for account_key in self.account_keys:
            xml_content = self._get_jira_xml_report(account_key)
            if xml_content:
                tree = ET.fromstring(xml_content)
                total_account_hours = convert_to_float(get_project_hours(tree))
                account_name = get_billing_name(tree)
                tickets = populate_tickets(tree)
                account = BillingAccount(self.client_name, account_key,
                                         account_name, total_account_hours)
                account.add_tickets(tickets)
                accounts.append(account)
        return accounts


def get_project_hours(tree):
    """ This function will return the total project hours from the JIRA XML report.
    """
    return tree.find('./project_hours').text


def get_billing_name(tree):
    """ This function will return the billing name from the JIRA XML report.
    """
    return tree.find('./billing_name').text


def populate_tickets(tree):
    """ This function will populate the tickets from the JIRA XML report."""
    tickets = []
    for activity in tree.findall('./activities/activity'):
        for issues in activity.findall('issues'):
            for issue in issues.findall('issue'):
                ticket_key = issue.find('issue_key').text
                ticket_summary = issue.find('issue_summary').text
                ticket_hours = convert_to_float(issue.find('issue_hours').text)
                tickets.append(Tickets(ticket_key, ticket_summary, ticket_hours))
    return tickets


def convert_to_float(str_number):
    """ This function will convert the string number to float.
    """
    return round(float(str_number), 2)


class PDFReportGenerator:
    """ Class responsible for generating the PDF report.
    """
    PDF_OPTIONS = {
        'page-size': 'A4',
        'margin-top': '10mm',
        'margin-right': '0mm',
        'margin-bottom': '10mm',
        'margin-left': '0mm',
        'encoding': 'UTF-8',
    }

    def __init__(self, report) -> None:
        self.report = report
        self.client_name = self.report.client_name
        self.to_date = self.report.to_date
        self.from_date = self.report.from_date
        self.accounts = self.report.fetch_report_information()

    def generate(self):
        """
        Turn the report into a PDF using its template.
        """
        if not self.accounts:
            LOGGER.info('No accounts found for %s', self.client_name)
            return None
        template = get_template('{template}/{template}.html'.format(template="report"))
        invoice = template.render({
            'site': Site.objects.get_current(),
            'client_name': self.client_name,
            'total_hours': self.get_total_hours(),
            'to_date': self.to_date,
            'from_date': self.from_date,
            'accounts': self.accounts,
        })
        pdf_path = os.path.join(settings.INVOICE_PDF_PATH, '{}.pdf'.format(uuid.uuid4()))
        pdf_configuration = pdfkit.configuration(wkhtmltopdf=settings.HTML_TO_PDF_BINARY_PATH)
        pdfkit.from_string(invoice, pdf_path, configuration=pdf_configuration, options=self.PDF_OPTIONS)
        return pdf_path

    def get_total_hours(self):
        """ This function will return the total hours for the report.
        """
        total_hours = 0.0
        for account in self.accounts:
            total_hours += account.total_hours
        return total_hours
