# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2018 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for client report.
"""

from unittest import TestCase
from unittest.mock import MagicMock, patch
import os
import xml.etree.ElementTree as ET

from accounting.invoice.client_report import (
    BillingAccount,
    PDFReportGenerator,
    Report,
    Tickets,
    convert_to_float,
    get_billing_name,
    get_project_hours,
    populate_tickets,
)


XML_CONTENT = b'''<?xml version="1.0" encoding="UTF-8"?>
<project>
    <project_key>BB,MNG,UQ</project_key>
    <project_name>Bebop</project_name>
    <project_owner />
    <project_responsible>XA</project_responsible>
    <billing_name>UQE</billing_name>
    <client_name />
    <project_category />
    <project_status>3</project_status>
    <billing_key>UQXSUPPORT</billing_key>
    <project_hours>3.3166664</project_hours>
    <report_period>
        <report_start>2023-08-01</report_start>
        <report_end>2023-08-30</report_end>
    </report_period>
    <users></users>
    <activities>
        <activity>
            <activity_name></activity_name>
            <activity_hours>3.3166664</activity_hours>
            <issues>
                <issue>
                    <issue_id>0</issue_id>
                    <issue_key>BB-4591</issue_key>
                    <issue_summary>UQ support requests</issue_summary>
                    <issue_reporter>DA</issue_reporter>
                    <issue_reporter_full>DA</issue_reporter_full>
                    <issue_hours>3.2499998</issue_hours>
                    <worklogs></worklogs>
                </issue>
                <issue>
                    <issue_id>0</issue_id>
                    <issue_key>BB-7444</issue_key>
                    <issue_summary>Migrate infrastructure to replace learnx with extend in names for
                        staging</issue_summary>
                    <issue_reporter>CC</issue_reporter>
                    <issue_reporter_full>MB</issue_reporter_full>
                    <issue_hours>0.06666667</issue_hours>
                    <worklogs></worklogs>
                </issue>
            </issues>
        </activity>
    </activities>
</project>'''


class TestReport(TestCase):
    """ Test for client report. """
    def setUp(self):
        """ Set up test objects. """
        self.report = Report('2022-01-01', '2022-01-31', 'Client A', ['account1', 'account2'])

    @patch('accounting.invoice.client_report.requests.get')
    def test_fetch_report_information(self, mock_get):
        """ Test fetch report information. """
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.content = XML_CONTENT
        mock_get.return_value = mock_response

        accounts = self.report.fetch_report_information()

        self.assertEqual(len(accounts), 2)
        self.assertIsInstance(accounts[0], BillingAccount)
        self.assertIsInstance(accounts[0].tickets[0], Tickets)

    def test_get_project_hours(self):
        """ Test get project hours. """
        xml_content = b'<report><project_hours>10.5</project_hours></report>'
        tree = ET.fromstring(xml_content)
        project_hours = get_project_hours(tree)
        self.assertEqual(project_hours, '10.5')

    def test_get_billing_name(self):
        """ Test get billing name. """
        xml_content = b'<report><billing_name>Account A</billing_name></report>'
        tree = ET.fromstring(xml_content)
        billing_name = get_billing_name(tree)
        self.assertEqual(billing_name, 'Account A')

    def test_populate_tickets(self):
        """ Test populate tickets. """
        xml_content = b'''<report>
<activities>
    <activity>
        <issues>
            <issue>
                <issue_key>KEY-123</issue_key>
                <issue_summary>Summary</issue_summary>
                <issue_hours>5.5</issue_hours>
            </issue>
        </issues>
    </activity>
</activities></report>'''
        tree = ET.fromstring(xml_content)
        tickets = populate_tickets(tree)
        self.assertEqual(len(tickets), 1)
        self.assertEqual(tickets[0].key, 'KEY-123')
        self.assertEqual(tickets[0].summary, 'Summary')
        self.assertEqual(tickets[0].hours, 5.5)

    def test_convert_to_float(self):
        """ Test convert to float."""
        self.assertEqual(convert_to_float('5.5'), 5.5)


class TestPDFReportGenerator(TestCase):
    """ Test for PDF report generator. """
    def setUp(self):
        """ Set up test objects. """
        self.report = Report('2022-01-01', '2022-01-31', 'Client A', ['account1', 'account2'])
        self.pdf_path = None

    @patch('accounting.invoice.client_report.requests.get')
    def test_generate(self, mock_get):
        """ Test generate pdf report. """
        mock_response = MagicMock()
        mock_response.status_code = 200
        xml_content = XML_CONTENT
        mock_response.content = xml_content
        mock_get.return_value = mock_response

        generator = PDFReportGenerator(self.report)
        self.pdf_path = generator.generate()
        self.assertIsNotNone(self.pdf_path)
        self.assertTrue(os.path.exists(self.pdf_path))
        self.assertEqual(generator.get_total_hours(), 6.64)

    def tearDown(self):
        # Delete the report file after running the test
        if os.path.exists(self.pdf_path):
            os.remove(self.pdf_path)
