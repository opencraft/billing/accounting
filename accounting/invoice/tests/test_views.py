# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for Invoice views.
"""

from unittest import mock
import datetime

from django.core import mail
from django.core.mail.message import EmailMessage
from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
import freezegun

from accounting.account.tests.factories import AccountFactory
from accounting.authentication.tests.factories import UserFactory
from accounting.invoice import choices, models
from accounting.invoice.tests import factories

NOW = datetime.datetime(2018, 1, 10, 20, 31, 3, 350993, tzinfo=timezone.utc)


class InvoiceViewSetTestCase(APITestCase):
    """ Test cases for `views.InvoiceViewSet`. """

    @freezegun.freeze_time(NOW)
    def setUp(self):
        """ Set up test objects. """
        self.admin = UserFactory(is_staff=True)
        self.provider_account = AccountFactory(business_name='Developer', user__username='developer')
        self.client_account = AccountFactory(business_name='OpenCraft GmbH', user__username='opencraft')
        self.invoice = factories.InvoiceFactory(provider=self.provider_account, client=self.client_account, date=NOW)
        super().setUp()

    def test_approve(self):
        """ Approving an invoice through the appropriate URL is considered manual approval. """
        # First visit approves the invoice.
        url = reverse('invoice:invoice-approve', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'approved': True})
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.is_approved)
        self.assertEqual(self.invoice.approved, choices.InvoiceApproval.manually)

        # Second visit when already approved doesn't change anything.
        url = reverse('invoice:invoice-approve', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {'detail': 'Not found.'})
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.is_approved)
        self.assertEqual(self.invoice.approved, choices.InvoiceApproval.manually)

    @freezegun.freeze_time(NOW)
    @mock.patch.object(models.Invoice, 'upload_to_google_drive')
    @mock.patch.object(models.Invoice, 'fill_line_items_from_jira')
    @mock.patch.object(EmailMessage, 'attach_file')
    @mock.patch.object(models.Invoice, 'to_pdf')
    def test_regenerate(self, mock_to_pdf, mock_attach_file, mock_fill_line_items_from_jira,
                        mock_upload_to_google_drive):
        """ Regenerate invoice through appropriate URL """
        # Mock.
        pdf_path = 'https://drive.google.com/invoice.pdf'
        mock_upload_to_google_drive.return_value = {'alternateLink': pdf_path}
        mock_to_pdf.return_value = pdf_path

        # Unapproved invoice regenerated thourgh URL.
        url = reverse('invoice:invoice-regenerate', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'invoice_regenerated': True})
        self.assertEqual(mock_to_pdf.call_count, 2)
        self.assertEqual(mock_attach_file.call_count, 1)
        self.assertEqual(mock_fill_line_items_from_jira.call_count, 1)
        self.assertEqual(mock_upload_to_google_drive.call_count, 1)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Approve your invoice for December')

        # Body contains proper invoice approval URL and text.
        body = (
            "Hello,\n"
            "\n"
            "I have prepared your invoice for December! It is attached and ready for you to review.\n"
            "\n"
            "When you've looked it over, can you approve it by clicking on this link, "
            "or by visiting it in your browser?\n"
            "\n"
            "https://billing.opencraft.com/invoice/{uuid}/approve/\n"
            "\n"
            "In case you want to regenerate this invoice to include newer JIRA logs, click on the link below.\n"
            "\n"
            "https://billing.opencraft.com/invoice/{uuid}/regenerate/\n"
            "\n"
            "If you find any other issues with your invoice, please contact {client_email} before January 5th -- "
            "I will consider your invoice approved by then, and will proceed to sending the corresponding payment.\n"
        )
        invoices = models.Invoice.objects.filter(date__month=NOW.month)
        self.assertEqual(
            mail.outbox[0].body,
            body.format(uuid=invoices.first().uuid, client_email=self.client_account.user.email)
        )
        self.invoice.approved = choices.InvoiceApproval.manually
        self.invoice.save()

        # Approved invoice cannot be regenerated through URL.
        url = reverse('invoice:invoice-regenerate', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {'detail': 'Not found.'})

    def test_pay(self):
        """ Paying an invoice through the appropriate URL marks the invoice as paid. """
        self.client.force_authenticate(user=self.admin)

        # First visit pays the invoice.
        url = reverse('invoice:invoice-pay', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data.get('paid'))
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.paid)

        # Second visit when already paid doesn't change anything.
        url = reverse('invoice:invoice-pay', [self.invoice.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.data, {'detail': 'Not found.'})
        self.invoice.refresh_from_db()
        self.assertTrue(self.invoice.paid)
