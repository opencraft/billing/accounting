# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Configuration ###############################################################
# Any configuration variable can be overridden with `VARIABLE = VALUE` in a
# git-ignored `private.mk` file.

.PHONY: static

.DEFAULT_GOAL := help
DOCKER_BUILDKIT ?= 1
DOCKER_REGISTRY := "${CI_REGISTRY}/opencraft/billing/accounting"
HELP_SPACING ?= 30
PY := python3
PY_MANAGE := $(PY) manage.py
HONCHO_MANAGE := honcho run $(PY_MANAGE)
HONCHO_TEST := honcho -e .envs/.local/.test run
HONCHO_MANAGE_LOW_PRIORITY="HUEY_QUEUE_NAME=accounting_low_priority $(HONCHO_MANAGE)"
HONCHO_MANAGE_TEST := $(HONCHO_TEST) $(PY_MANAGE) --noinput -v2
HONCHO_MANAGE_TEST_COVERAGE := $(HONCHO_TEST) coverage run manage.py test --noinput -v2
PACKAGE_NAME := accounting
PIPENV := $(PY) -m pipenv
SHELL ?= /bin/bash
WORKERS ?= 3
WORKERS_LOW_PRIORITY ?= 3

# Parameters ##################################################################

# For `test.one` use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),test.one manage ci-build-image ci-retag-django-image ci-push-images))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

# Commands ####################################################################

help: ## Display this help message.
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-$(HELP_SPACING)s\033[0m %s\n", $$1, $$2}'

clean: cov.clean ## Remove all temporary files.
	find -name '*.pyc' -delete
	find -name '*~' -delete
	find -name '__pycache__' -type d -delete
	rm -rf \
		.coverage \
		.ipython \
		.mypy_cache \
		.pytest_cache \
		build \
		celerybeat.pid \
		htmlcov \
		staticfiles

install_requirements:  ## Install depenedencies needed for production
	$(PIPENV) install --system

install_dev_requirements:  ## Install depenedencies needed for development.
	$(PIPENV) install --dev --system

static: clean ## Collect static files for production.
	$(HONCHO_MANAGE) collectstatic --noinput

shell: ## Run a management command.
	$(HONCHO_MANAGE_LOW_PRIORITY) shell_plus

manage: ## Run a management command.
	$(HONCHO_MANAGE) $(RUN_ARGS)

manage.low-priority: ## Run a management command.
	$(HONCHO_MANAGE_LOW_PRIORITY) $(RUN_ARGS)

migrate: clean ## Run migrations.
	$(HONCHO_MANAGE) migrate

migrations.check: clean ## Check for unapplied migrations
	!(($(HONCHO_MANAGE) showmigrations | grep '\[ \]') && printf "\n\033[0;31mERROR: Pending migrations found\033[0m\n\n")

migrations: clean ## Generate migrations.
	$(HONCHO_MANAGE) makemigrations

translations: ## Make .po files for all existing languages.
	$(HONCHO_MANAGE) makemessages --all -v1

translations.compile: ## Compile .mo files from existing .po files.
	$(HONCHO_MANAGE) compilemessages

# CI/CD #######################################################################

ci-build-image:
	docker build \
		-t "${DOCKER_REGISTRY}:$(RUN_ARGS)-$(CI_COMMIT_SHORT_SHA)" -t "${DOCKER_REGISTRY}:$(RUN_ARGS)-latest" \
		-f ./compose/production/$(RUN_ARGS)/Dockerfile .

ci-retag-django-image:
	@for service in $(RUN_ARGS) ; do \
		docker tag "${DOCKER_REGISTRY}:django-$(CI_COMMIT_SHORT_SHA)" "${DOCKER_REGISTRY}:$$service-$(CI_COMMIT_SHORT_SHA)" && \
		docker tag "${DOCKER_REGISTRY}:django-latest" "${DOCKER_REGISTRY}:$$service-latest" ; \
	done

ci-push-images:
	@for service in $(RUN_ARGS) ; do \
		docker push "${DOCKER_REGISTRY}:$$service-$(CI_COMMIT_SHORT_SHA)" && \
		docker push "${DOCKER_REGISTRY}:$$service-latest" ; \
	done

# Formatting ##################################################################

format: clean ## Run code formatting and import sorting.
	black $(PACKAGE_NAME) --line-length=120
	isort $(PACKAGE_NAME)

# Development #################################################################

start: clean migrate migrations.check static translations.compile ## Run the accounting service in a production setting with concurrency.
	honcho start --port 1786 -f /Procfile.web

start.huey: clean ## Run the accounting service in a production setting with concurrency.
	honcho start -f /Procfile.huey --concurrency "worker=$(WORKERS),worker_low_priority=$(WORKERS_LOW_PRIORITY)"

# Tests #######################################################################

test.pre-commit: ## Run pre-commit tests.
	pre-commit run --all

test.quality: clean ## Run quality tests.
	mypy $(PACKAGE_NAME) && \
	flake8 $(PACKAGE_NAME) && \
	pylint $(PACKAGE_NAME)

test.unit: clean ## Run all unit tests.
	$(HONCHO_MANAGE_TEST_COVERAGE)

test.migrations: clean ## Check if migrations are missing.
	@$(HONCHO_MANAGE) makemigrations --dry-run --check

test: clean test.quality test.unit test.migrations cov.html ## Run all tests.
	@echo "\nAll tests OK!\n"

test.one: clean ## Run one test or test suite.
	$(HONCHO_MANAGE_TESTS) $(RUN_ARGS)

# Files #######################################################################

cov.html: ## Generate html coverage
	coverage html
	@echo "\nCoverage HTML report at file://`pwd`/build/coverage/index.html\n"
	@coverage report

cov.clean:
	coverage erase

# We ignore `private.mk` so you can define your own make targets, or override some.
include *.mk

# Include `private.mk` last to make sure to override anything.
ifneq ("$(wildchar private.mk)","")
	include private.mk
endif
